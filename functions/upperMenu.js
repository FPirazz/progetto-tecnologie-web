$(document).ready(function(){

    let shopDropdown = 0;    
    let valuesDropdown = 0;



    $("#shopDropdown").click(function(){
        if(!shopDropdown){
            $("#collapseShopLarge").animate({
                marginTop: '6.5%'
            });

            shopDropdown = 1;
        } else {
            $("#collapseShopLarge").animate({
                marginTop: '-20%'
            });

            shopDropdown = 0;
        }
    });

    $("#valuesDropdown").click(function(){
        if(!valuesDropdown){
            $("#collapseValuesLarge").animate({
                marginTop: '6.5%'
            });

            valuesDropdown = 1;
        } else {
            $("#collapseValuesLarge").animate({
                marginTop: '-20%'
            });

            valuesDropdown = 0;
        }
    });


});
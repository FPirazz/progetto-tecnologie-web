function generateProducts(products){
  
    let allProducts = `<section>
                         <div class="container"> 
                            <div class="row">    
    `;
    
    for(let i=0; i < products.length; i++){
       
        let product = `
        
                    <div class="col-12 col-md-6">
                        <a href="product.php?idClothes=${products[i]["idproduct"]}"><img src="${products[i]["imgproduct"]}" alt=""></a> 
                        <p>${products[i]["name"]}</p>
                        <p>${products[i]["price"]} €</p>
                    </div>
              
            
        `;
        allProducts += product;
      
    }
    allProducts = allProducts +  `
        </div>
       </div> 
     </section>  
    `;
    return allProducts;
}
$(document).ready(function(){
     //function -> what to do after json receive data 
    $.getJSON("api-product.php", function(data){
    
         const products = generateProducts(data);
         //console.log(data);
         $("main").append(products);
    });

})

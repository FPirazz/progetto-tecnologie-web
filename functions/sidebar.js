$(document).ready(function(){

    let closeOverlay = 1;    

    $("div.overlay > *").hide();


    $("#menuTrigger").click(function(){
        if(closeOverlay == 1){
            $("div.overlay > *").show();
            $("div.overlay > *").animate({
                marginLeft: '0px'
            });
            
            $("body > *").animate({
                backgroundColor: '#e6e6e6'
            });

            $("div.popup > *").animate({
                backgroundColor: '#FFFFFF'
            });

            // $("div.overlay").slideToggle();

    
            closeOverlay = 0;
            console.log(closeOverlay);
        } 
    });


    $("#exitOver").click(function(){
        console.log("DIOPOCO")
        if(closeOverlay == 0) {
            $("div.overlay > *").animate({
                marginLeft: '-80%'
            });

            $("body > *").animate({
                backgroundColor: '#FFFFFF'
            });

            $("div.popup > *").animate({
                backgroundColor: '#FFFFFF'
            });

            closeOverlay = 1;
            console.log(closeOverlay);
        }
    });

});
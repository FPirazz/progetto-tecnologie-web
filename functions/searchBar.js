$(document).ready(function(){

    let searchOpen = 0;

    $("#searchTrigger").click(function(){
        if(!searchOpen){
            $("header input[type='text']").animate({
                marginLeft: '0px'
            });
            searchOpen = 1;
        } else {
            $("header input[type='text']").animate({
                marginLeft: '-60%'
            });
            searchOpen = 0;
        }
    });

})
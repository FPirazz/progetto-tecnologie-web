
<?php

require_once("./bootstrap.php");
$templateParams["titolo"] = "Log out";

$templateParams["nome"] = "loading.php";


if(isset($_POST["firstName"]) && isset($_POST["lastName"]) && isset($_POST["username"]) && isset($_POST["email"]) && isset($_POST["password"]) ){
    

   
    $password = $_POST['password']; 
    // Crea una chiave casuale
    $random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
    // Crea una password usando la chiave appena creata.
    $password = hash('sha512', $password.$random_salt);

    
    
    
    $active=1;
    $dbh->insertUsers($_POST["firstName"],$_POST["lastName"],$_POST["email"],$_POST["username"],$password,$random_salt,$active);

    $email = $_POST['email'];
    $password = $_POST['password'];
    
    // Recupero la password criptata.
    if(login($email, $password,$dbh->getDb()) == true) {
       // Login eseguito
      
    } else {
       
       // Login fallito
       header('Location: ./login.php?error=1');
    }
    
    
    
 }


if (isset($_GET["action"]) && $_GET["action"]==1){
  
        $_SESSION = array();

        $params = session_get_cookie_params();
        // Cancella i cookie attuali.
        setcookie(session_name(), '', time() - 42000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
        // Cancella la sessione.
        session_destroy();
        
        
       
}




require("template/base.php");

?>




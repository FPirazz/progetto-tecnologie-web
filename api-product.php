<?php
require_once("./bootstrap.php");

$products =  $dbh->getnewArrivals();

for($i=0; $i < count($products); $i++){
    $products[$i]["imgproduct"] = UPLOAD_DIR.$products[$i]["imgproduct"];
}

header("Content-Type: application/json");
echo json_encode($products);

?>
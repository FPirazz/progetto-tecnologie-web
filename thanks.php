<?php

require_once("./bootstrap.php");
$templateParams["titolo"] = "Thanks";
$templateParams["nome"] = "info.php";

foreach($_SESSION["cart"] as $items){
    $dbh->decreaseProduct($items);
    $dbh->insertBag($_SESSION["user_id"], $items);

    $prods = $dbh->getProductById($items);

    $templateParams["outtaStock"] = $dbh->getOutStock($items);
    if(!empty($templateParams["outtaStock"])){
        $message = "For all Sellers: Item " . $prods[0]["name"] . " is out of stock!";
    
        $dbh->insertNotif(1, $message);
    }

    

    $messageUser= "Thank you for your patronage! The following item has been shipped to you: " . $prods[0]["name"];

    $dbh->insertNotif($_SESSION["user_id"], $messageUser);
}

$_SESSION["cart"] = array();





require("template/base.php");

?>
<?php

require_once("./bootstrap.php");
$templateParams["titolo"] = "Products";

$templateParams["nome"] = "multiple-pieces.php";

$templateParams["allProdsCat"] = $dbh->getAllProducts();

$templateParams["categories"] = fillDropdownMenu($templateParams["allProdsCat"]);

$templateParams["filter"] = "";

if(isset($_GET["nameFilter"])){
    $templateParams["allProd"] = $dbh->getProductByName($_GET["nameFilter"]);
}


if(isset($_GET["clothesFilter"])){
    $templateParams["vars"] = "?clothesFilter=";
    $templateParams["filter"] = $_GET["clothesFilter"];

} 

if(isset($_GET["sexFilter"])){
    $templateParams["sexFilter"] = $_GET["sexFilter"];

    $templateParams["vars"] = $templateParams["vars"] . $templateParams["filter"] . "&sexFilter=" . $templateParams["sexFilter"];

    if ((strcmp($templateParams["filter"], "none") == 0)
    && (strcmp($templateParams["sexFilter"], "none") == 0)) {
        $templateParams["allProd"] = $dbh->getAllProducts();

    } else if(strcmp($templateParams["filter"], "none") == 0){
        $templateParams["allProd"] = $dbh->getProductBySex($templateParams["sexFilter"]);

    } else if(strcmp($templateParams["sexFilter"], "none") == 0){
        $templateParams["allProd"] = $dbh->getProductByCategory($templateParams["filter"]);

    } else {
        $templateParams["allProd"] = $dbh->getProductBySexAndCat($templateParams["sexFilter"], $templateParams["filter"]);
    }

} 

$id_requered = -1;
//da fare anche caso in cui id non esiste
if (isset($_GET["id"])){
    $id_requered = $_GET["id"];
}

require("template/base.php");




?>
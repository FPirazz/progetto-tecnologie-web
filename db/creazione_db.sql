-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3300
-- Creato il: Gen 06, 2021 alle 04:09
-- Versione del server: 10.4.14-MariaDB
-- Versione PHP: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `webshop`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `bag`

--

CREATE USER 'sec_user'@'localhost' IDENTIFIED BY 'eKcGZr59zAa2BEWU';
GRANT SELECT, INSERT, DELETE, UPDATE ON `webshop`.* TO 'sec_user'@'localhost';


CREATE TABLE `bag` (
  `idbag` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `product` int(11) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `bag`
--

INSERT INTO `bag` (`idbag`, `user`, `product`, `date`) VALUES
(1, 1, 1, '2021-01-05'),
(2, 2, 3, '2021-01-06'),
(13, 2, 5, '2021-01-06'),
(14, 2, 2, '2021-01-06'),
(15, 2, 8, '2021-01-06'),
(16, 2, 8, '2021-01-06'),
(17, 2, 8, '2021-01-06'),
(18, 2, 8, '2021-01-06'),
(19, 2, 5, '2021-01-06'),
(20, 2, 8, '2021-01-06');

-- --------------------------------------------------------

--
-- Struttura della tabella `notifications`
--

CREATE TABLE `users` (
  `iduser` int(11) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
   `email` varchar(100) NOT NULL, 
  `username` varchar(100) NOT NULL,
  `password` varchar(512) NOT NULL,
  `salt` char(128) NOT NULL,
  `active` tinyint(4) DEFAULT 0,
   `seller` tinyint(4) DEFAULT 0 
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `notifications` (
  `id` int(11) NOT NULL,
  `subject` int(11) NOT NULL,
  `text` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `notifications`
--

INSERT INTO `notifications` (`id`, `subject`, `text`) VALUES
(1, 1, 'Item ran out of stock'),
(4, 1, 'For all Sellers: Item 1is out of stock!');

-- --------------------------------------------------------

--
-- Struttura della tabella `product`
--

CREATE TABLE `product` (
  `idproduct` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `imgproduct` varchar(100) NOT NULL,
  `color` varchar(100) NOT NULL,
  `sex` varchar(100) NOT NULL,
  `category` varchar(50) NOT NULL,
  `size` varchar(100) NOT NULL,
  `price` int(11) NOT NULL,
  `date` date NOT NULL,
  `accessory` tinyint(4) DEFAULT 0,
  `quantity` tinyint(4) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `product`
--

INSERT INTO `product` (`idproduct`, `name`, `imgproduct`, `color`, `sex`, `category`, `size`, `price`, `date`, `accessory`, `quantity`) VALUES
(1, 'Puffer Jacket', 'PUFFER JACKET.jpg', 'black', 'man', 'Jacket', 'L', 30, '2021-01-05', 0, 42),
(2, 'Top with Full Sleeves', 'MATCHING TOP WITH FULL SLEEVES.jpg', 'black', 'women', 'Top', 'M', 15, '2021-01-05', 0, 75),
(3, 'Sweatshirt', 'SWEATSHIRT.jpg', 'pink', 'kids', 'Sweatshirt', 'L', 22, '2021-01-05', 0, 89),
(4, 'Camicia Casual', 'CAMICIA CASUAL.jpg', 'green', 'man', 'Shirt', 'S', 25, '2021-01-05', 0, 44),
(5, 'Christmas Sweater', 'CHRISTMAS JUMPER.jpg', 'brown', 'women', 'Sweater', 'L', 15, '2021-01-05', 0, -7),
(6, 'Cravatta', 'CRAVATTA.jpg', 'blue', 'man', 'Accessorie', 'S', 12, '2021-01-05', 1, 87),
(7, 'Skirt with buttons', 'GONNA CON BOTTONI,jpg', 'ma che colore è', 'women', 'Skirt', 'S', 22, '2021-01-05', 0, 113),
(8, 'Bikini Top', 'TOP BIKINI.jpg', 'ma che colore è', 'women', 'Bikini', 'S', 6, '2021-01-05', 0, 0);


CREATE TABLE `login_attempts` (
  `user_id` INT(11) NOT NULL,
  `time` VARCHAR(30) NOT NULL 
) ENGINE=InnoDB;




-- --------------------------------------------------------

--
-- Struttura della tabella `users`
--

--
-- Dump dei dati per la tabella `users`
--

INSERT INTO `users` (`iduser`, `firstname`, `lastname`, `email`, `username`, `password`, `active`, `seller`) VALUES
(1, 'giulio', 'mondini', 'gmondini@gmail.com', 'giuliom', '123456', 1, 1),
(2, 'francesco', 'rossi', 'frossi@gmail.com', 'fra1999', 'chica', 1, 0),
(3, 'teresa', 'gatti', 'tgatti@gmail.com', 'cat', '2020', 0, 0),
(4, 'anna', 'tassinari', 'atassinari@gmail.com', 'tassorosso', 'password', 1, 0);

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `bag`
--
ALTER TABLE `bag`
  ADD PRIMARY KEY (`idbag`);

--
-- Indici per le tabelle `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`idproduct`);

--
-- Indici per le tabelle `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`iduser`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `bag`
--
ALTER TABLE `bag`
  MODIFY `idbag` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT per la tabella `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT per la tabella `product`
--
ALTER TABLE `product`
  MODIFY `idproduct` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT per la tabella `users`
--
ALTER TABLE `users`
  MODIFY `iduser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php
class DatabaseHelper{
    private $db;

    public function __construct($servername, $username, $password, $dbname, $port){
        //give php possibility to read errors from msyqli
       // mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
        $this->db = new mysqli($servername, $username, $password, $dbname, $port);
        if ($this->db->connect_error) {
            die("Connection failed: " . $db->connect_error);
        }        
    }

    public function getDb(){
        return $this->db;
    }

    public function getUsers(){
    
        $stmt = $this->db->prepare("SELECT `iduser`, `firstname`,`lastname`,`email`,`username`, `password`, `active`,`seller` FROM users ");
        $stmt->execute();
        $result = $stmt->get_result();
        $stmt->close();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getProducts(){
        $stmt = $this->db->prepare("SELECT * FROM product");
        $stmt->execute();
        $result = $stmt->get_result();
        $stmt->close();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getRandomProducts($n){
        $stmt = $this->db->prepare("SELECT idproduct, `name`, imgproduct, color, sex, category, price,`date`,`accessory`, `quantity` FROM product ORDER BY RAND() LIMIT ?");
        $stmt->bind_param('i',$n);
        $stmt->execute();
        $result = $stmt->get_result();
        $stmt->close();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

   

    public function getBags(){
        $stmt = $this->db->prepare("SELECT * FROM bag");
        $stmt->execute();
        $result = $stmt->get_result();
        $stmt->close();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    
    public function getBagbyIdUser($iduser){
        
        $query = "SELECT b.idbag , p.name, p.price, p.category, p.sex, p.color, p.date , p.imgproduct  FROM bag b, product p WHERE b.user = ? and p.idproduct = b.product  ORDER BY b.`date` DESC";
     
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i',$iduser);
        $stmt->execute();
        $result = $stmt->get_result();
        $stmt->close();

        return $result->fetch_all(MYSQLI_ASSOC);
    }



    public function getAllProducts(){
        $query = "SELECT * FROM product";
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $stmt->close();

        return $result->fetch_all(MYSQLI_ASSOC);
    }
    

    public function getProductByColor($color){
        $query = "SELECT idproduct, `name`, imgproduct, color, sex, category, price,`date`,`accessory`, `quantity` FROM product WHERE color=? ";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s',$id);
        $stmt->execute();
        $result = $stmt->get_result();
        $stmt->close();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getProductByName($name){
        $query = "SELECT idproduct, `name`, imgproduct, color, sex, category, price,`date`,`accessory`, `quantity` FROM product WHERE INSTR(`name`, ?) > 0";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s',$name);
        $stmt->execute();
        $result = $stmt->get_result();
        $stmt->close();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getProductBySex($sex){
        $query = "SELECT idproduct, `name`, imgproduct, color, sex, category, price, `date`,`accessory`,`quantity` FROM product WHERE sex=? ";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s',$sex);
        $stmt->execute();
        $result = $stmt->get_result();
        $stmt->close();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getProductBySexAndCat($sex, $cat){
        $query = "SELECT idproduct, `name`, imgproduct, color, sex, category,price, `date`,`accessory`,`quantity` FROM product WHERE sex=? AND category=?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ss',$sex, $cat);
        $stmt->execute();
        $result = $stmt->get_result();
        $stmt->close();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getProductById($id){
        $query = "SELECT idproduct, `name`, imgproduct, color, sex, category,size, price,`date`,`accessory`, `quantity` FROM product WHERE idproduct=? ";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i',$id);
        $stmt->execute();
        $result = $stmt->get_result();
        $stmt->close();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getNotifsId($id){
        $query = "SELECT id, text FROM notifications WHERE subject=? ";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i',$id);
        $stmt->execute();
        $result = $stmt->get_result();
        $stmt->close();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function removeNotById($id){
        $query = "DELETE FROM notifications WHERE id=? ";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i',$id);
        $stmt->execute();
        $result = $stmt->get_result();
        $stmt->close();

    }

    public function removeProductbyId($id){
        $query = "DELETE FROM product WHERE idproduct=? ";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i',$id);
        $stmt->execute();
        $result = $stmt->get_result();
        $stmt->close();

    }

    public function getProductByCategory($category){
        $query = "SELECT idproduct, `name`, imgproduct, color, sex, category, price,`date`,`accessory`, `quantity` FROM product WHERE category=? ";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s',$category);
        $stmt->execute();
        $result = $stmt->get_result();
        $stmt->close();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getnewArrivals(){
        $query = "SELECT idproduct, `name`, imgproduct, color, sex,category, price,`date`,`accessory`, `quantity` FROM product WHERE DATEDIFF(CURDATE(),`date`)<30";
        $stmt = $this->db->prepare($query);
       
        $stmt->execute();
        $result = $stmt->get_result();
        $stmt->close();
        return $result->fetch_all(MYSQLI_ASSOC);
    }
   
    public function getOutStock($id){
        $query = "SELECT name FROM product WHERE idproduct = ? AND quantity = 0";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i',$id);
       
        $stmt->execute();
        $result = $stmt->get_result();
        $stmt->close();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getAdmins(){
        $query = "SELECT iduser, firstname, lastname, email, username, password, active, seller FROM users WHERE seller = 1";
        $stmt = $this->db->prepare($query);
       
        $stmt->execute();
        $result = $stmt->get_result();
        $stmt->close();
        return $result->fetch_all(MYSQLI_ASSOC);
    }
  

    public function insertUsers( $firstname, $lastname, $email, $username, $password,$salt, $active){
        $query = "INSERT INTO users (firstname, lastname, email, username, `password`,salt, active) VALUES (?, ?,?,?, ?,?,?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ssssssi',$firstname, $lastname, $email, $username, $password,$salt, $active);
        $stmt->execute();
        
        $stmt->close();
        
        
    }
    public function insertProduct($name, $imgproduct, $color, $sex, $category,$price, $date, $accessory,$quantity){
        $query = "INSERT INTO product (`name`, imgproduct, color, sex, category,price, date, accessory, quantity) VALUES (?, ?,?,?, ?,?,?,?,?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('sssssssss',$name, $imgproduct, $color, $sex, $category,$price, $date, $accessory,$quantity);
        $stmt->execute();
        
        $stmt->close();
        
        
    }

    public function insertNotif($user, $text){
        $query = "INSERT INTO notifications (subject, text) VALUES (?, ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('is', $user, $text);
        $stmt->execute();
        
        $stmt->close();
        
        
    }

    public function insertBag($user, $prodId){
        $query = "INSERT INTO bag (user, product, date) VALUES (?, ?, CURDATE())";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ii', $user, $prodId);
        $stmt->execute();
        
        $stmt->close();
        
    }

    public function updateProduct($idproduct, $name, $imgproduct, $color, $sex, $category,$price, $date, $accessory,$quantity){
        $query = "UPDATE product SET idproduct = ?, `name` = ?, imgproduct = ?, color = ?,sex = ?, category = ?, price = ?,`date` = ?, accessory = ? , quantity = ? WHERE idproduct= ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('isssssisiii',$idproduct, $name, $imgproduct, $color, $sex, $category,$price, $date, $accessory,$quantity,$idproduct);
        
        return $stmt->execute();
    }

    public function decreaseProduct($idproduct){
        $query = "UPDATE product SET quantity = quantity - 1 WHERE idproduct = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i',$idproduct);
        $stmt->execute();
        
        $stmt->close();
    }

    public function checkLogin($email, $password){
        $query = "SELECT iduser, username, password, active , seller from `users` WHERE `email` = ? AND `password` = ? AND `active` = 1";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ss',$email, $password);
        $stmt->execute();
        
        $result = $stmt->get_result();
      //  $stmt->close();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

   
}
?>
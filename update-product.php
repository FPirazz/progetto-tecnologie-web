<?php
require_once("bootstrap.php");
if(!isSellerLogged()){
    header("location: login.php");
}
if(isset($_GET["action"]) && ($_GET["action"]=="6")){
    
    $dbh->removeProductbyId($_GET["id"]);
    header("location: login.php");
}

if(isset($_POST["action"]) && !($_POST["action"]=="")){
    //modifico
   $productid = $_POST["action"];
   $name = $_POST["productname"];
   $color = $_POST["productcolor"];
   $sex = $_POST["productsex"];
   $size = $_POST["productsize"];
   $category = $_POST["productcategory"];
   $price = $_POST["productprice"];
   $date = date("Y-m-d");
   $accessory = $_POST["productaccessory"];
   $quantity = $_POST["productquantity"];
   
   if(isset($_FILES["imgproduct"]) && strlen($_FILES["imgarticolo"]["name"])>0){
  
      list($result, $msg) = uploadImage(UPLOAD_DIR, $_FILES["imgproduct"]);
      
      if($result != 0){

          $imgproduct = $msg;
         
          $dbh->updateProduct($productid,$name, $imgproduct, $color, $sex, $category,$price, $date, $accessory,$quantity);
       }
    }else{
        $imgproduct= $_POST["oldimg"];
        $dbh->updateProduct($productid,$name, $imgproduct, $color, $sex, $category,$price, $date, $accessory,$quantity);
      
      
   
    }
   header("location: index.php");


}
if(isset($_POST["action"]) && ($_POST["action"]=="")){
    
   
   $name = $_POST["productname"];
   $color = $_POST["productcolor"];
   $sex = $_POST["productsex"];
   $size = $_POST["productsize"];
   $category = $_POST["productcategory"];
   $price = $_POST["productprice"];
   $date = date("Y-m-d");
   $accessory = $_POST["productaccessory"];
   $quantity = $_POST["productquantity"];

   list($result, $msg) = uploadImage(UPLOAD_DIR, $_FILES["imgproduct"]);
   
   if($result != 0){

        $imgproduct = $msg;
       
        $dbh->insertProduct( $name, $imgproduct, $color, $sex, $category,$price, $date, $accessory,$quantity);
   }
   header("location: index.php");

}


?>
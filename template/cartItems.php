<section>
    <?php foreach($_SESSION["cart"] as $cart): ?>
    <?php $product = $dbh->getProductById($cart); ?>
    
    <?php foreach($product as $prod): ?>
        <div class="cartImg">
            <a href="product.php?idClothes=<?php echo $prod["idproduct"] ?>">
            <img src="<?php echo UPLOAD_DIR.$prod["imgproduct"];?>" alt="">
            </a>

            <div class="cartP">
                <p><?php echo $prod["name"] ?></p>
                <p><?php echo $prod["price"] ?> EUR</p>
            </div>
            <div class="deleteItem">
                <form action="" method="get">
                    <input type="submit" name="<?php echo $prod["idproduct"] ?>" id="<?php echo $prod["idproduct"] ?>" value="X">
                </form>
            </div>

        </div>
        <?php $tot = $tot + $prod["price"] ?>
        <?php endforeach; ?>
    <?php endforeach; ?>

    <div class="buyButton">
        <p>Total: <?php echo $tot ?> EUR</p>

        <form action="thanks.php" method="get">
            <input type="submit" name="buy" id="buy" value="Buy">
        </form>
    </div>

</section>
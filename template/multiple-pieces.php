<section>
    <form action="./products.php<?php echo $templateParams["vars"] ?>" method="GET">
    <label for="clothesFilter">Filter: </label>
        <select name="clothesFilter" id="clothesFilter">
            <option value="none">---</option>
            <?php foreach($templateParams["categories"] as $cat): ?>
                <option value="<?php echo $cat ?>"><?php echo $cat ?></option>

            <?php endforeach; ?>
        </select>
        <select name="sexFilter" id="sexFilter">
            <option value="none">---</option>
            <option value="Man">Man</option>
            <option value="Women">Women</option>

        </select>
        <input type="submit" value="Apply" id="filterButton">
    </form>

    <div class="grid-container"> 
        <?php foreach($templateParams["allProd"] as $prods): ?> 
            <div class="grid-item">
                <a href="product.php?idClothes=<?php echo $prods["idproduct"] ?>"><img src="<?php echo UPLOAD_DIR.$prods["imgproduct"];?>" alt="">
                <p><?php echo $prods["name"]; ?></p>
                <p><?php echo $prods["price"]; ?> €</p></a>
            </div>

            
        <?php endforeach; ?>
    </div>
</section>
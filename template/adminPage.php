<section>
       
  <div class="d-flex justify-content-center">
          <a class="btn btn-outline-dark" href="login.php?action=1" role="button">Stock</a>
          <a class="btn btn-outline-dark" href="change-product.php?action=4" role="button">New</a>
          <a class="btn btn-outline-dark" href="login.php?action=3" role="button">Notifications</a>
          <a class="btn btn-outline-dark" href="logout.php?action=1" role="button">Log out</a>
  </div>  
    
            
        
    <?php if($templateParams["view"] == "Stock"): ?>
        <div class="container">
         <div class="row">   
          <?php foreach($templateParams["allProdsCat"] as $articolo): ?> 
          <div class="col-12 col-md-6">
            <table >
              <thead class="text-center border">
                <tr>
                    <th scope="col">Foto</th><th scope="col">IdProduct</th><th scope="col">Name</th><th scope="col">price</th><th scope="col">quantity</th><th scope="col">changes</th>
                </tr>
              </thead> 
              <tbody class="text-center border"> 
                
                <tr class="border">
                    
                    <td scope="row"><img src="<?php echo UPLOAD_DIR.$articolo["imgproduct"]; ?>"  alt="" /></td>
                    
                    
                    <td><?php echo $articolo["idproduct"]?></td>
                    <td><?php echo $articolo["name"]?></td>
                    <td><?php echo $articolo["price"]?></td>
                    <td><?php echo $articolo["quantity"]?></td>
                    <td>
                        <a href="change-product.php?action=3&id=<?php echo $articolo["idproduct"]; ?>">change</a>
                
                    </td> 
                </tr>
    
              </tbody>  
            </table>  
          </div>
          <?php endforeach; ?> 
         </div> 
        </div>  
    <?php endif; ?>

    <?php if($templateParams["view"] == "Notifications"): ?>
      <?php if(isset($templateParams["notfs"])): ?>
        <?php foreach($templateParams["notfs"] as $notif): ?>
          <div class="container">
            <div class="row">
              <div class="col">
                <p><?php print_r($notif["text"]) ?></p>
              </div>
              <div class="col">
                <form action="login.php?action=3" method="get">
                      <input type="submit" name="notf<?php echo $notif["id"] ?>" id="notf<?php echo $notif["id"] ?>" value="X">
                </form>
              </div>
            </div>
          </div>
        <?php endforeach; ?>
      <?php endif; ?>

    <?php endif; ?>
          
   
</section>
<section>    
     <div class="d-flex justify-content-center">
          <a class="btn btn-outline-dark" href="login.php?action=1" role="button">History</a>
          <a class="btn btn-outline-dark" href="login.php?action=2" role="button">Bag</a>
          <a class="btn btn-outline-dark" href="login.php?action=3" role="button">Notifications</a>
          <a class="btn btn-outline-dark" href="logout.php?action=1" role="button">Log out</a>
     
     </div>
     <?php if($templateParams["view"] == "Order Hystory"): ?>
          <?php foreach($templateParams["bag"] as $prod): ?>
                  <!-- <div class="singleProd">  -->
               <div class="row col-md-6 mt-3">
                    <div class="col-12 pt-3 text-center border"><p><?php echo "ORDER NUMBER: ".$prod["idbag"] ?></p><p><?php echo $prod["name"] ?></p></div>
                    <div class="col-6 border"><img src="<?php echo UPLOAD_DIR.$prod["imgproduct"];?>"  alt=""></div> 
                    <div class="col-6 text-center border"><p><?php echo $prod["category"] ?></p><p><?php echo $prod["color"] ?></p><p><?php echo $prod["price"] ?>EUR</p><p><?php echo $prod["date"] ?></p></div>   
                                 
               </div>
          <?php endforeach; ?>           
     <?php endif; ?>
     
     <?php if($templateParams["view"] == "Bag"): ?>

          <?php foreach($_SESSION["cart"] as $cart): ?>
          <?php $product = $dbh->getProductById($cart); ?>
          
          <?php foreach($product as $prod): ?>
               <div class="cartImg">
                    <a href="product.php?idClothes=<?php echo $prod["idproduct"] ?>">
                    <img src="<?php echo UPLOAD_DIR.$prod["imgproduct"];?>" alt="">
                    </a>

                    <div class="cartP">
                         <p><?php echo $prod["name"] ?></p>
                         <p><?php echo $prod["price"] ?> EUR</p>
                    </div>
                    <div class="deleteItem">
                         <form action="login.php?action=2" method="get">
                              <input type="submit" name="prod<?php echo $prod["idproduct"] ?>" id="prod<?php echo $prod["idproduct"] ?>" value="X">
                         </form>
                    </div>

               </div>
               <?php $tot = $tot + $prod["price"] ?>
               <?php endforeach; ?>
          <?php endforeach; ?>

          <div class="buyButton">
               <p>Total: <?php echo $tot ?> EUR</p>

               <form action="thanks.php" method="get">
                    <input type="submit" name="buy" id="buy" value="Buy">
               </form>
          </div>

     <?php endif; ?>

     <?php if($templateParams["view"] == "Notifications"): ?>
          <?php if(isset($templateParams["notfs"])): ?>
               <?php foreach($templateParams["notfs"] as $notif): ?>
               <div class="container">
                    <div class="row">
                         <div class="col">
                              <p><?php print_r($notif["text"]) ?></p>
                         </div>
                         <div class="col">
                              <form action="login.php?action=3" method="get">
                                   <input type="submit" name="notf<?php echo $notif["id"] ?>" id="notf<?php echo $notif["id"] ?>" value="X">
                              </form>
                         </div>
                    </div>
               </div>
               <?php endforeach; ?>
          <?php endif; ?>

     <?php endif; ?>

</section>




         
 
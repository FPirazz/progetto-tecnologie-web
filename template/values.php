<session>
  <?php switch($templateParams["titolo"]):
      case "Our-Partners" :?>
       <div class="partnersPage">
              <div class="partnersTitle1">
                     <p>I = CHANGE</p>
              </div>

              <div class="partnersDesc1">
                     <p>Brand. is dedicated to a fair and more sustainable future 
                     and is delighted to be working with i=change in support of 
                     protecting women, our land and our reef by donating $1 of 
                     every sale to one of the three charities, chosen by you. </p>
              </div>
              <br>
              <div class="partnersTitle2">
                     <p>THE BATCHELOR INSTITUTE</p>
              </div>

              <div class="partnersDesc2">
                     <p>Brand. and The Batchelor Institute have held a mutual 
                     alliance since 2018. <br>
                     After visiting The Northern Territory, Brand. Directors, Federico 
                     Pirazzoli and Sergiu Gabriel Rolnic wanted to give back to the land 
                     and communities that have given so much. As a brand built 
                     through technology, the partnership with an institute anchored 
                     in technology was a no brainer.</p>
              </div>
       </div>

      <?php break; ?>
 <?php case "Work-Ethics" :?>
   
       <div class="ethicsPage">
              <div class="ethicsTitle1">
                     <p>FROM THE DIRECTORS</p>
              </div>

              <p>As Directors of a contemporary fashion label, 
              we recognise our responsibility to operate in a 
              manner that is empowering of all people who come into
              contact with our supply chain while being conscious 
              of our planet.</p>

              <p>We are dedicated to working alongside our partners 
              to ensure responsible business practices are in place to 
              reflect our fundamental brand values of integrity and 
              transparency.</p>

              <p>We are committed to helping create change in an 
              industry that can too often be exploitative, as well 
              as continually educating ourselves on social and 
              environmental practices that are considerate of our 
              makers, our planet and our customers. From our 
              overseas manufacturers to our international stores 
              we strive for our core values at Brand. to be reflected 
              on every level.</p>

       </div>

        <?php break; ?>
 <?php endswitch; ?>    

</session>

    
<session>
  <?php switch($templateParams["titolo"]):
      case "Size-Guide" :?>

      <div class="sizeSettings">
         <p>Men</p>

         <img src="./upload/menGuide.png" alt="">

         <p>Women</p>

         <img src="./upload/womenGuide.png" alt="">
      </div>

      <?php break; ?>
 <?php case "Terms-Condition" :?>
   <div class="termsSettings">
      <p>These terms set out the agreement for online trading 
      between Brand the label Pty Ltd (ABN 13600 747 680) 
      ("Brand the label PTY LTD") and the user of this website 
      ("You") Please read them carefully before proceeding with 
      entering the website. You must accept these terms and 
      conditions to access this website any further.</p>

      <p>This website is owned and operated 
      by Brand the label PTY LTD.</p>

      <p>PLACING AN ORDER AND PAYMENT</p>
      <p>By completing an online order, You are making an offer to 
      purchase the product at the price on our website.</p>

      <p>Payment must be made at the time of submitting the online 
      order. Payment can be made by Visa, MasterCard, Amex, 
      Afterpay, or PayPal only. You warrant that you are an 
      authorised user of the credit card, Afterpay account or 
      PayPal account used to complete payment.</p>

      <p>All online orders placed through this website are subject to 
      confirmation and acceptance by SIR the label. We will email you 
      with confirmation of the receipt of your online order. Sir the 
      label may reject Your online order for any reason and refund 
      any payment made by you, at any time prior to dispatch.</p>

      <p>The online order must be paid in full immediately upon entry 
      into the Agreement. All transactions on the Australian and 
      New Zealand website are settled in Australian Dollars (AUD). 
      Customers purchasing from New Zealand will have their 
      currency automatically converted to Australian Dollars when 
      you complete the transaction. The conversion rate varies from 
      day to day with the current exchange rate. Many banks also 
      charge a small currency conversion fee, contact your bank or 
      credit provider for details.</p>

      <p>Brand the label’s prices may vary in the event of price changes 
      or mistakes on this website without prior notice to you. If 
      SIR the label requests payment for increased prices, you may 
      cancel the order. Notification of your wish to cancel the 
      order in these circumstances must be received within seven (7) 
      business days of the announcement of the increase.</p>

      <p>FAULTY POLICY</p>
      We aim to provide you with products of the highest standard and quality. If you have received a product with a manufacturing fault, please contact us as soon as you receive your order. This way we can guide you through the returns process and help resolve the problem. Please provide our team with images of the damage, defect or fault for a preliminary assessment.


      <p>If the product is confirmed to have a manufacturing fault, 
      we will replace the product or refund the price of the product 
      to your original payment method at your request. If the product 
      is found not to have a defect or deemed out of warranty, we will 
      ship the product back to you.</p>


      <p>It does not constitute a defect, if in our reasonable 
      opinion, the product has, following the sale to you, become 
      of unacceptable quality due to fair wear and tear, misuse or 
      failure to use in accordance with care for instructions.</p>

      <p>Please see our full Returns Policy for more information.</p>
   </div>
        <?php break; ?>
 <?php case "Privacy-Policy" :?>
      <div class="privacySettings">
         <p>Updated November 2020.</p>

         <p>We know that you care how information about you is used and shared, and we appreciate your trust. 
            This Privacy Policy explains how Brand the label (collectively, “we,” “us” or “our”) collect, 
            use and share information about you when you use our websites and online services (“Services”) or otherwise interact with us.</p>

         <p>Please read this Privacy Policy carefully and contact us if you have any questions - our contact details can be found on our Contact Page.</p>

         <p>If you provide us with your personal information then this indicates that you have had sufficient opportunity to access, 
         and have read and accepted, this Privacy Policy. If you do not wish to provide personal information to us, 
         then you do not have to do so, however it may affect your use of this website or any products and services offered on it.</p>

         <p>COLLECTION OF PERSONAL INFORMATION</p>

         <p>Personal Information: The types of personal information we collect will depend on the purposes(s) for which we are collecting it. For example, we may ask for:</p>

         <ul>
            <li>name;</li>
            <li>contact details including email address, address and telephone number;</li>
            <li>marital status and anniversary;</li>
            <li>date of birth;</li>
            <li>demographic information such as postcode;</li>
            <li>credit card or other payment details (if you are buying product from us);</li>
            <li>preferences and opinions; and</li>
            <li>any other information requested on this website or otherwise requested by us or provided by you.</li>
         </ul>
         
         <p>Non-identifiable information: 
         Data that have never been labelled with individual identifiers or from which identifiers 
         have been permanently removed, and by means of which no specific individual can be identified. When you visit our website, 
         we automatically record non-identifiable information that your browser sends. This data may include:</p>
         
         <ul>
            <li>your computer's IP address;</li>
            <li>browser type;</li>
            <li>webpage you were visiting before you came to our website</li>
            <li>the pages within www.brand.com you visit</li>
            <li>the time spent on those pages, items, and information searched on our website, access times, dates and other statistics.</li>
         </ul>

         <p>Non-identifiable information is collected for analysis and 
         evaluation in order to help us improve our website and the services and products we provide. This data will not be 
         used in association with any other personal information.</p>
      </div>

    <?php break; ?>
 <?php case "Stores" :?>

   <div class="storesSettings">
      <img src="./upload/negozioFisico.jpg" alt="">
      <p>Brand Styling Shop</p>
      <p>Via Esempio, 23 - 47521</p>
   </div>

    <?php break; ?>
<?php case "Thanks" :?>

<p>Thanks for your purchase!</p>

 <?php break; ?>
 <?php endswitch; ?>    

</session>

    
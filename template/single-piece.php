<section>
      
    <?php foreach($templateParams["product"] as $prod): ?>
        <div class="singleProd">
            <img src="<?php echo UPLOAD_DIR.$prod["imgproduct"];?>" alt="">
        
            <p><?php echo $prod["name"] ?></p>
            <p><?php echo $prod["price"] ?> EUR</p>
            <p><?php echo $prod["category"] ?></p>
            <p><?php echo $prod["sex"] ?></p>
            <p><?php echo $prod["color"] ?></p>

        </div>
    <?php endforeach; ?>

    <form action="" method="post" target="_self">
        <?php if(login_check($dbh->getDb()) && $templateParams["product"][0]["quantity"] > 0): ?>
            <input type="submit" class="addToCart" value="Add to cart" name="addCart" id="addCart">        
        <?php else: ?>
            <input type="submit" class="addToCart" value="Add to cart" disabled>
        <?php endif ?>
    </form>

</section>
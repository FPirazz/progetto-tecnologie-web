

<!--session con tabella di login -->
<div class="container-fluid">

    <div class="row">
        <div class="col-sm" id="leftColumn">
            <div class="loginForm">
                <form action="login.php" method="POST">
                    
                    <h2>Sign in</h2>
                    <?php if(isset($templateParams["failAccess"])): ?>
                               <p><?php echo $templateParams["failAccess"]; ?></p>
                    <?php endif; ?>
                    <ul>
                        <li>
                            <div class="userInput">
                                <input type="text" id="email" name="email" required/>
                                <label id="emailLabel" for="email">Email</label>
                            </div>
                        </li>
                        <li>
                            <div class="passInput">
                                <input type="password" id="password" name="password"/>
                                <label id="passLabel" for="password">Password</label>
                            </div>
                        </li>
                        <li>
                            <div class="buttonContainerLogIn">
                                <input type="submit" name="submit" value="Log In" onclick=formhash(this.form, this.form.password) id="buttonInput"/>
                            </div>
                        </li>
                    </ul>            
                </form>
            </div>
        </div>
    
        <div class="col-sm">
            <div class="signUpForm">
                <form action="logout.php" method="POST">
                        
                    <h2>Sign Up</h2>
                    <ul>
                        <li>
                            <div class="userInput">
                                <input type="text" id="firstName" name="firstName" required/>
                                <label id="userLabel" for="firstName">First Name</label>
                            </div>
                        </li>        
                        <li>
                            <div class="userInput">
                                <input type="text" id="lastName" name="lastName" required/>
                                <label id="userLabel" for="lastName">Last Name</label>
                            </div>
                        </li>   

                        <li>
                            <div class="userInput">
                                <input type="text" id="username" name="username" required/>
                                <label id="username" for="username">Username</label>
                            </div>
                        </li> 
                        <li>
                            <div class="userInput">
                                <input type="text" id="email" name="email" required/>
                                <label id="email" for="email">Email</label>
                            </div>
                        </li>
                        <li>
                            <div class="passInput">
                                <input type="password" id="password" name="password"/>
                                <label id="passLabel" for="password">Password</label>
                            </div>
                        </li>
                        <li>
                            <div class="buttonContainerSignUp">
                                <input type="submit" name="submit" value="Sign Up" onclick=formhash(this.form, this.form.password) id="buttonInput"/>
                            </div>
                        </li>
                    </ul>            
                </form>
            </div> 
        </div> 
    </div> 
</div>



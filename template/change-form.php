
<?php if (isset($_GET["id"])): ?>

    
  <section>
   <div class="d-flex justify-content-center">
       <a class="btn btn-outline-dark" href="update-product.php?action=6&id=<?php echo $_GET["id"]; ?>" role="button">REMOVE PRODUCT</a> 
   
        
   </div>  
     <form action="update-product.php" method="POST" enctype="multipart/form-data">
          <div class="container" >
              <div class="row justify-content-center">
                  <div class="col-12 col-md-7">
                       <p>Product Id: <?php echo $templateParams["product"]["idproduct"]; ?></p>
                  </div>
                 
                  <div class="col-12 col-md-7">
                       <p>Name</p>
                  </div>
                  <div class="col-12 col-md-7 mb-3">
                      <input type="text" class="form-control" id="productname" name="productname" placeholder="name" value="<?php echo $templateParams["product"]["name"]; ?>" />
                      
                  </div>
                  <div class="col-12 col-md-7 ">
                       <p>Color</p>
                  </div>
                  <div class="col-12 col-md-7 mb-3">
                      <input type="text" class="form-control" id="productcolor" name="productcolor" placeholder="color" value="<?php echo $templateParams["product"]["color"]; ?>" />
                      
                  </div>
                  <div class="col-12 col-md-7">
                       <p>Sex</p>
                  </div>
                  <div class="col-12 col-md-7  mb-3">
                      <input type="text" class="form-control" id="productsex" name="productsex" placeholder="sex" value="<?php echo $templateParams["product"]["sex"]; ?>" />
                      
                  </div>
                  <div class="col-12 col-md-7">
                       <p>Category</p>
                  </div>
                  <div class="col-12 col-md-7 mb-3">
                      <input type="text" class="form-control" id="productcategory" name="productcategory" placeholder="category" value="<?php echo $templateParams["product"]["category"]; ?>" />
                      
                  </div>
                  <div class="col-12 col-md-7">
                       <p>Size</p>
                  </div>
                  <div class="col-12 col-md-7 mb-3">
                      <input type="text" class="form-control" id="productsize" name="productsize" placeholder="size" value="<?php echo $templateParams["product"]["size"]; ?>" />
                      
                  </div>
                  <div class="col-12 col-md-7">
                       <p>Price</p>
                  </div>
                  <div class="col-12 col-md-7 mb-3">
                      <input type="text" class="form-control" id="productprice" name="productprice" placeholder="price" value="<?php echo $templateParams["product"]["price"]; ?>" />
                      
                  </div>
                  <div class="col-12 col-md-7">
                       <p>Accessory</p>
                  </div>
                  <div class="col-12 col-md-7 mb-3">
                      <input type="text" class="form-control" id="productaccessory" name="productaccessory" placeholder="accessory" value="<?php echo $templateParams["product"]["accessory"]; ?>" />
                      
                  </div>
                  <div class="col-12 col-md-7">
                       <p>Quantity</p>
                  </div>
                  <div class="col-12 col-md-7 mb-3">
                      <input type="text" class="form-control" id="productquantity" name="productquantity" placeholder="quantity" value="<?php echo $templateParams["product"]["quantity"]; ?>" />
                      
                  </div>
                  <div class="col-12 col-md-7 mb-3">
                    <input type="file" name="imgproduct" id="imgproduct" value="<?php echo UPLOAD_DIR.$templateParams["product"]["imgproduct"]; ?>" />
                    <img src="<?php echo UPLOAD_DIR.$templateParams["product"]["imgproduct"]; ?>" class="img-fluid" alt="" />  
                  </div>
                  <div class="col-12 col-md-7 mb-3">
                     <input type="submit" name="submit" value="SEND " />
                    <a href="login.php">Back</a>
                  </div> 
                
            
              </div>
          </div>
          <input type="hidden" name="action" value="<?php echo $templateParams["product"]["idproduct"]; ?>" />
          <input type="hidden" name="oldimg" value="<?php echo $templateParams["product"]["imgproduct"]; ?>" />
          
      </form>   
  </section> 
<?php else : ?>
    <section>
     
     <form action="update-product.php" method="POST" enctype="multipart/form-data">
          <div class="container" >
              <div class="row justify-content-center">
                  
                 
                  <div class="col-12 col-md-7">
                       <p>Name</p>
                  </div>
                  <div class="col-12 col-md-7 mb-3">
                      <input type="text" class="form-control" id="productname" name="productname" placeholder="name" value="<?php echo $templateParams["product"]["name"]; ?>" />
                      
                  </div>
                  <div class="col-12 col-md-7">
                       <p>Color</p>
                  </div>
                  <div class="col-12 col-md-7 mb-3">
                      <input type="text" class="form-control" id="productcolor" name="productcolor" placeholder="color" value="<?php echo $templateParams["product"]["color"]; ?>" />
                      
                  </div>
                  <div class="col-12 col-md-7">
                       <p>Sex</p>
                  </div>
                  <div class="col-12 col-md-7 mb-3">
                      <input type="text" class="form-control" id="productsex" name="productsex" placeholder="sex" value="<?php echo $templateParams["product"]["sex"]; ?>" />
                      
                  </div>
                  <div class="col-12 col-md-7">
                       <p>Category</p>
                  </div>
                  <div class="col-12 col-md-7 mb-3">
                      <input type="text" class="form-control" id="productcategory" name="productcategory" placeholder="category" value="<?php echo $templateParams["product"]["category"]; ?>" />
                      
                  </div>
                  <div class="col-12 col-md-7">
                       <p>Size</p>
                  </div>
                  <div class="col-12 col-md-7 mb-3">
                      <input type="text" class="form-control" id="productsize" name="productsize" placeholder="size" value="<?php echo $templateParams["product"]["size"]; ?>" />
                      
                  </div>
                  <div class="col-12 col-md-7 ">
                       <p>Price</p>
                  </div>
                  <div class="col-12 col-md-7 mb-3">
                      <input type="text" class="form-control" id="productprice" name="productprice" placeholder="price" value="<?php echo $templateParams["product"]["price"]; ?>" />
                      
                  </div>
                  <div class="col-12 col-md-7">
                       <p>Accessory</p>
                  </div>
                  <div class="col-12col-md-7  mb-3">
                      <input type="text" class="form-control" id="productaccessory" name="productaccessory" placeholder="accessory" value="<?php echo $templateParams["product"]["accessory"]; ?>" />
                      
                  </div>
                  <div class="col-12 col-md-7">
                       <p>Quantity</p>
                  </div>
                  <div class="col-12 col-md-7 mb-3">
                      <input type="text" class="form-control" id="productquantity" name="productquantity" placeholder="quantity" value="<?php echo $templateParams["product"]["quantity"]; ?>" />
                      
                  </div>
                  <div class="col-12 col-md-7 mb-3">
                    <input type="file" name="imgproduct" id="imgproduct" />
                    <img src="<?php echo UPLOAD_DIR.$templateParams["product"]["imgproduct"]; ?>" class="img-fluid" alt="" />  
                  </div>
                  <div class="col-12 col-md-7 mb-3">
                     <input type="submit" name="submit" value="SEND " />
                    <a href="login.php">Back</a>
                  </div> 
                
            
              </div>
          </div>
          <input type="hidden" name="action" value="<?php echo ""; ?>" />
          
      </form>   
  </section> 
<?php endif; ?>
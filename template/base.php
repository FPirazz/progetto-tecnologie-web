<!DOCTYPE html>
<html lang="en">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale-1.0" />
         <title>Webshop-<?php $templateParams["titolo"];?></title> 

        <link href="./fontawesome-free-5.15.1-web/css/all.css" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
        <link href="./css/style.css" rel="stylesheet">
        <script src="./functions/sidebar.js" type="text/javascript"></script>
        <script src="./functions/upperMenu.js" type="text/javascript"></script>
        <script src="./functions/searchBar.js" type="text/javascript"></script>
        <script src="./functions/cartMenu.js" type="text/javascript"></script>
        
        
        <?php
        if(isset($templateParams["js"])):
            foreach($templateParams["js"] as $script):
        ?>
        <script src="<?php echo $script; ?>"></script>
        <?php
              endforeach;
        endif;
        ?>

    </head>
    <body>

    

        <header>


            <div class="completeOverlay">
                <div class="overlay">
                    <div class="popup">
                        <div class="exitMenu">
                            <i class="fas fa-times" id="exitOver"></i>
                        </div>
                        <h2>Brand</h2>
                        <ul>
                            <li><a href="./newArrivals.php">New Arrivals</a> <i class="fas fa-chevron-right"></i></li>
                            
                            <li><a data-toggle="collapse" href="#collapseShop" role="button" aria-expanded="false" aria-controls="collapseExample">Shop</a></li>
                            <div class="collapse" id="collapseShop">
                                <ul>
                                    <!-- TEMP DA SOSTITUIRE CON CATEGORIE -->
                                    <a href="./products.php?clothesFilter=none&sexFilter=none"><li>All</li></a>
                                    <a href="./products.php?clothesFilter=none&sexFilter=man"><li>Men</li></a>
                                    <a href="./products.php?clothesFilter=none&sexFilter=women"><li>Women</li></a>
                                </ul>
                            </div>

                            <li><a href="">Collection</a> <i class="fas fa-chevron-right"></i></li>

                            <li><a data-toggle="collapse" href="#collapseValues" role="button" aria-expanded="false" aria-controls="collapseExample">Values</a></li>
                            <div class="collapse" id="collapseValues">
                                <ul>
                                    <a href="./ourPartners.php"><li>Our Partners</li></a>
                                    <a href="./workEthics.php"><li>Work Ethics</li></a>
                                </ul>
                            </div>

                            </ul>
                        <hr>
            

                    </div>
                </div>

                <div class="menuIcon">
                    <i class="fas fa-bars" id="menuTrigger">
                    </i>
                </div>

            </div>

            <div class="shopIcon" id="cartTrigger">
                <a href="./login.php?action=2">   
                    <i class="fas fa-shopping-bag" id="temp"></i>
                </a>
            </div>

            <form action="products.php?nameFilter=<?php echo $_GET['nameFilter']; ?>">
                <input type="text" id="nameFilter" name="nameFilter">
            </form>

            <div class="searchIcon" id="searchTrigger">
                <i class="fas fa-search"></i>
            </div>
            
            <div class="userIcon">
                <a href="login.php"><i class="far fa-user-circle"></i></a>
            </div>     
            
            <h1><a href="./index.php">Brand</a></h1>   
          

            <div class="longWidthHeader">

                <div class="d-flex justify-content-center">
                    <a href="./newArrivals.php">New Arrivals</a>
                        
                        <a id="shopDropdown" data-toggle="collapse" href="#collapseShopLarge" role="button" aria-expanded="false" aria-controls="collapseExample">
                        Shop
                        </a>    
                        <div class="collapse" id="collapseShopLarge">
                        <!-- TEMP DA SOSTITUIRE CON CATEGORIE -->
                        <a href="./products.php?clothesFilter=none&sexFilter=none"><li>All</li></a>
                            <a href="./products.php?clothesFilter=none&sexFilter=man"><li>Men</li></a>
                            <a href="./products.php?clothesFilter=none&sexFilter=women"><li>Women</li></a>
                        </div>

                    <a href="">Collection</a>

                    <a id="valuesDropdown" data-toggle="collapse" href="#collapseValues" role="button" aria-expanded="false" aria-controls="collapseExample">
                        Values
                    </a>    
                    <div class="collapse" id="collapseValuesLarge">
                        <ul>
                            <a href="./ourPartners.php"><li>Our Partners</li></a>
                            <a href="./workEthics.php"><li>Work Ethics</li></a>
                        </ul>
                    </div>
                </div>

            </div>


        </header>


       <main>
        <?php
         if(isset($templateParams["nome"])){
            require($templateParams["nome"]);
         }
        ?> 
       </main> 
       
  






        <div class="container">
        <footer>

            <div class="footerSmallWidth">
                <div class="customerCare">
                    
                    <a data-toggle="collapse" href="#collapseFirst" role="button" aria-expanded="false" aria-controls="collapseExample">
                    <i class="fas fa-plus"></i>
                    Customer care
                    </a>
                
                    <div class="collapse" id="collapseFirst">
                        <ul>
                            <a href="./contact.php"><li>Contact</li></a>
                            <a href="./shipping.php"><li>Shipping & Delivery</li></a>
                            <a href="./returns.php"><li>Returns</li></a>
                        </ul>
                    </div>
                </div>

                <div class="info">
                    
                    <a data-toggle="collapse" href="#collapseSecond" role="button" aria-expanded="false" aria-controls="collapseExample">
                    <i class="fas fa-plus"></i>
                    Info
                    </a>
                
                    <div class="collapse" id="collapseSecond">
                        <ul>
                            <a href="./stores.php">Stores</a>
                            <a href="./size-guide.php">Size Guide</a>
                            <a href="./privacy-policy.php">Privacy Policy</a>
                            <a href="./terms-condition.php">Terms & Condition</a>
                        </ul>
                    </div>
                </div>

                <div class="followUs">
                    
                    <a data-toggle="collapse" href="#collapseThird" role="button" aria-expanded="false" aria-controls="collapseExample">
                    <i class="fas fa-plus"></i>
                    Follow Us
                    </a>
                
                    <div class="collapse" id="collapseThird">
                        <ul>
                            <a href="https://it-it.facebook.com/"><i class="fab fa-facebook-square"></i> <li>Facebook</li></a>
                            <a href="https://www.instagram.com/"><i class="fab fa-instagram"></i> <li>Instagram</li></a>
                        </ul>
                    </div>
                </div>

                
                <div class="finalInfo">
                    <p>Info@tempEmail.com <br>
                    +39 F-A-K-E N-U-M</p> <br>
                    
                    <p>We're based in Cesena, Italy</p> <br>
                    
                    <p>Customer Care Hours: <br>
                    Sun-Thurs | 3pm - 11pm GMT+1</p> <br>

                    <p>Worldwide Shipping</p>
                    

                </div>
            </div>

            <div class="footerLargeWidth">
                <div class="container">
                    <div class="row">
                        <div class="col-sm">
                            <div class="footerHeader">
                                <p>Customer Care</p>
                            </div>

                            <a href="./contact.php">Contact</a>
                            <a href="./shipping.php">Shipping & Delivery</a>
                            <a href="./returns.php">Returns</a>
                        </div>

                        <div class="col-sm">
                            <div class="footerHeader">
                                <p>Info</p>
                            </div>

                            <a href="./stores.php">Stores</a>
                            <a href="./size-guide.php">Size Guide</a>
                            <a href="./privacy-policy.php">Privacy Policy</a>
                            <a href="./terms-condition.php">Terms & Condition</a>
                        </div>

                        <div class="col-sm">
                            <div class="footerHeader">
                                <p>Follow Us</p>
                            </div>

                            <a href="https://it-it.facebook.com/"><i class="fab fa-facebook-square"></i> Facebook</a>
                            <a href="https://www.instagram.com/"><i class="fab fa-instagram"></i> Instagram</a>
                        </div>

                        <div class="col-sm">
                            <p>Info@tempEmail.com <br>
                            +39 F-A-K-E N-U-M</p> <br>
                            
                            <p>We're based in Cesena, Italy</p> <br>
                            
                            <p>Customer Care Hours: <br>
                            Sun-Thurs | 3pm - 11pm GMT+1</p> <br>

                            <p>Worldwide Shipping</p>
                        </div>
                    </div>
                </div>
            </div>

        </footer>

        </div>
        

    </body>
</html>
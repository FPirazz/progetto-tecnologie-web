<session>
   <?php switch($templateParams["titolo"]):
      case "Contact" :?>
         <div class="usaPar">
            <p>CUSTOMER CARE</p>
            <p>USA</p>
            <p>+61 F-A-K-E N-U-M-S</p>
            <p>Mon - Fri | 8am - 5pm AEST</p>
         </div>

         <div class="europePar">
            <p>EUROPE</p>
            <p>+39 F-A-K-E N-U-M</p>
            <p>Sun - Thurs | 3pm - 11pm GMT+1</p>
         </div>

      <?php break; ?>
   <?php case "Shipping" :?>
      <div class="shippinImage">
         <img src="./upload/shipping.png" alt="">
      </div>
      
      <div class="shippinTitle">
         <p>CHRISTMAS DELIVERY</p>
      </div>

      <div class="shippinDesc">
         <p>SIR. dispatch will close from midday Thursday 24th 
         December and commence again from Tuesday 29th December. 
         Please note that orders will be picked and packed in 
         order of being placed, our team will do the best they can 
         in sending out each order but please allow 2 - 3 days for 
         dispatch. </p>
      </div>

      <?php break; ?>
   <?php case "Returns" :?>
      <div class="returnTitle1">
         <p>RETURN POLICY</p>
      </div>
      
      <div class="returnDesc1">
         <p>We want you to be completely satisfied with your online 
         purchase.</p>

         <p>If you change your mind for any reason, you are welcome to return it 
         back to us within 30 days of receiving it.</p>
      </div>

      <div class="returnTitle2">
         <p>RETURNING CONDITION</p>
      </div>

      <div class="returnDesc2">
         <p>Item/s must be returned in original condition, unworn, 
         unaltered, unwashed and with their tags attached. If 
         merchandise is returned despite these conditions your 
         return will be sent back to you at an additional charge</p>
      </div>

      <?php break; ?>
   <?php endswitch; ?>    
</session>

    